KeePassX legacy
====================

This is a copy of KeePassX 0.4.4 source code that
you can also obtain here: https://www.keepassx.org/downloads/0-4
Commit 1187e9b08cc6e4a28772b560f68b38bc5c4616bc represents
an unmodified copy of the 0.4.4 archive you can find there.

I am not involved in the development of keepassx but since
I still use the 0.4 version, I want to maintain a copy of the
source that builds on my systems.

Feel free to look inside COPYING for license information.
The program as a whole is mostly subject to **GPL version 2**.

If you want to see what I changed, please look at the git commits
and the git log. My changes (all the code that I added) can be used
under the terms of the **MIT license**.

Install
--------------------

Install the dependencies (atm Qt4 is needed for the master branch
and Qt5 for the qt5 development branch). For Debian based systems
something like this should work:

	apt-get install qt4-default qt5-default # pick one
	apt-get install libx11-dev libx11-xcb-dev libxcb-xtest0-dev # for auto-type

Steps to make and install keepassx legacy (under /opt/keepassx):

	mkdir build
	cd build
	cmake -DCMAKE_INSTALL_PREFIX=/opt/keepassx ..
	make -j4
	sudo make install
	sudo ln -s /opt/keepassx/bin/keepassx /usr/local/bin/

Development
--------------------

### master branch
The master branch is very conservative and the software should
be pretty stable there. Keep in mind I am a one-man-army though.

#### Changelog

nothing really changed since fork from keepassx 0.4.4

### qt5 branch
I ported keepassx legacy to Qt5 recently and development will
continue on this branch until I think a new version can be
released.

#### Changelog

- RC4 code (that was used for in-memory encryption of passwords when the database is open) was removed and replaced with chacha20 code.
- ported to Qt5
